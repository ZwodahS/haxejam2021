# Overview

The game is created for HaxeJam 2021 using Haxe + Heaps along with my own personal library

# License

- Code license : MIT
- Assets : CC0
