/**
	Utils store all the functions that generally useful for the whole repo, but not yet lib/common level.

	For the globals variable, see Globals.hx
	For constants, see Constants.hx

**/
class Utils {
	public static function getRewardString(reward: Reward, amt: Int): String {
		switch (reward) {
			case Resource(t):
				switch (t) {
					case Food:
						return '${amt} Food';
					case Wood:
						return '${amt} Wood';
					case Stone:
						return '${amt} Stone';
				}
			default:
		}
		return "";
	}
}
