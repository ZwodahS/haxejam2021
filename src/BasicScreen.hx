class BasicScreen extends zf.Screen {
	public function new() {
		super();

		final anim = Assets.packed.assets.get("die:default").getAnim();
		anim.speed = 0.2;
		this.addChild(anim);
		anim.x = 50;
		anim.y = 50;
	}

	override public function update(dt: Float) {}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
