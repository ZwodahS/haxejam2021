import zf.ui.TextButton;

class MenuScreen extends zf.Screen {
	public function new() {
		super();

		final title = new h2d.Text(Assets.displayFont4x);
		title.text = "Dice Kingdom";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(30);
		this.addChild(title);

		var prev: h2d.Object = null;
		// @formatter:off
		final startGameButton = new TextButton(
			Colors.White[0], Colors.Greens[0], "Start Game",
			Assets.displayFont4x
		);
		startGameButton.setY(400).setX(Globals.game.gameWidth, AlignCenter);
		this.addChild(startGameButton);
		startGameButton.onLeftClick = startNewGame;
		prev = startGameButton;

	}

	override public function update(dt: Float) {}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}

	function startNewGame() {
		this.game.switchScreen(new GameScreen());
	}
}
