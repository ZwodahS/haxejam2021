/**
	Assets is used to store loaded assets
**/

import zf.ui.TileBoxFactory;
import zf.Assets.LoadedSpritesheet;

class Assets {
	public static var packed: LoadedSpritesheet;

	public static var defaultFont: h2d.Font;

	public static var displayFonts: Map<Int, h2d.Font>;
	public static var displayFont0x: h2d.Font;
	public static var displayFont1x: h2d.Font;
	public static var displayFont2x: h2d.Font;
	public static var displayFont3x: h2d.Font;
	public static var displayFont4x: h2d.Font;
	public static var displayFont5x: h2d.Font;

	public static var bodyRegularFonts: Map<Int, h2d.Font>;
	public static var bodyRegularFont0x: h2d.Font;
	public static var bodyRegularFont1x: h2d.Font;
	public static var bodyRegularFont2x: h2d.Font;
	public static var bodyRegularFont3x: h2d.Font;
	public static var bodyRegularFont4x: h2d.Font;
	public static var bodyRegularFont5x: h2d.Font;

	public static var bodyBoldFonts: Map<Int, h2d.Font>;
	public static var bodyBoldFont0x: h2d.Font;
	public static var bodyBoldFont1x: h2d.Font;
	public static var bodyBoldFont2x: h2d.Font;
	public static var bodyBoldFont3x: h2d.Font;
	public static var bodyBoldFont4x: h2d.Font;

	public static var brownBoxFactory: zf.ui.TileBoxFactory;
	public static var lightBrownBoxFactory: zf.ui.TileBoxFactory;
	public static var blueBoxFactory: zf.ui.TileBoxFactory;
	public static var greenBoxFactory: zf.ui.TileBoxFactory;
	public static var greyBoxFactory: zf.ui.TileBoxFactory;
	public static var greyBoxDottedFactory: zf.ui.TileBoxFactory;
	public static var redBoxFactory: zf.ui.TileBoxFactory;
	public static var whiteBoxFactory: zf.ui.TileBoxFactory;

	public static function load() {
		Assets.packed = zf.Assets.loadAseSpritesheetConfig('packed.json');

		Assets.displayFonts = new Map<Int, h2d.Font>();
		Assets.bodyRegularFonts = new Map<Int, h2d.Font>();
		Assets.bodyBoldFonts = new Map<Int, h2d.Font>();

		for (s in [8, 10, 12, 14, 17]) {
			Assets.displayFonts[s] = hxd.Res.load('fonts/gluten_medium_${s}.fnt')
				.to(hxd.res.BitmapFont)
				.toFont();
			Assets.bodyRegularFonts[s] = hxd.Res.load('fonts/montserrat_regular_${s}.fnt')
				.to(hxd.res.BitmapFont)
				.toFont();
			Assets.bodyBoldFonts[s] = hxd.Res.load('fonts/montserrat_bold_${s}.fnt')
				.to(hxd.res.BitmapFont)
				.toFont();
		}

		for (s in [20]) {
			Assets.displayFonts[s] = hxd.Res.load('fonts/gluten_medium_${s}.fnt')
				.to(hxd.res.BitmapFont)
				.toFont();
			Assets.bodyRegularFonts[s] = hxd.Res.load('fonts/montserrat_regular_${s}.fnt')
				.to(hxd.res.BitmapFont)
				.toFont();
		}

		Assets.displayFont0x = Assets.displayFonts[8];
		Assets.displayFont1x = Assets.displayFonts[10];
		Assets.displayFont2x = Assets.displayFonts[12];
		Assets.displayFont3x = Assets.displayFonts[14];
		Assets.displayFont4x = Assets.displayFonts[17];
		Assets.displayFont4x = Assets.displayFonts[20];

		Assets.bodyRegularFont0x = Assets.bodyRegularFonts[8];
		Assets.bodyRegularFont1x = Assets.bodyRegularFonts[10];
		Assets.bodyRegularFont2x = Assets.bodyRegularFonts[12];
		Assets.bodyRegularFont3x = Assets.bodyRegularFonts[14];
		Assets.bodyRegularFont4x = Assets.bodyRegularFonts[17];
		Assets.bodyRegularFont5x = Assets.bodyRegularFonts[20];

		Assets.bodyBoldFont0x = Assets.bodyBoldFonts[8];
		Assets.bodyBoldFont1x = Assets.bodyBoldFonts[10];
		Assets.bodyBoldFont2x = Assets.bodyBoldFonts[12];
		Assets.bodyBoldFont3x = Assets.bodyBoldFonts[14];
		Assets.bodyBoldFont4x = Assets.bodyBoldFonts[17];

		Assets.defaultFont = Assets.displayFont0x;

		// @formatter:off
		Assets.brownBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:brownbox").getTile(), 3, 3
		);
		Assets.lightBrownBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:lightbrownbox") .getTile(), 3, 3
		);
		Assets.blueBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:bluebox").getTile(), 3, 3
		);
		Assets.greyBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:greybox").getTile(), 3, 3
		);
		Assets.greyBoxDottedFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:greybox").getTile(1), 3, 3
		);
		Assets.greenBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:greenbox").getTile(), 3, 3
		);
		Assets.redBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:redbox").getTile(), 3, 3
		);
		Assets.whiteBoxFactory = new TileBoxFactory(
			Assets.packed.assets.get("ui:whitebox").getTile(), 3, 3
		);
	}

	public static function fromColor(color: Color, width: Int, height: Int): h2d.Bitmap {
		final bm = new h2d.Bitmap(Assets.packed.assets.get("white").getTile());
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}
}
