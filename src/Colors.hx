class Colors {
	public static final Browns: Array<Color> = [0xff9d6d51, 0xff6c483a];

	public static final Reds: Array<Color> = [0xffe67a73];

	public static final Greens: Array<Color> = [0xff5ca153];

	public static final Black: Array<Color> = [0xff111012];

	public static final White: Array<Color> = [0xfffffbe5];

	public static final Grey: Array<Color> = [0xff615f59, 0xff88867c];
}
