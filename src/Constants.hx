/**
	Constants are constants value / magic numbers for the game.
	These should be set to be final.

	For the evil globals counterpart, see Globals.hx
	For the function counterpart, see Utils.hx
**/
class Constants {
	public static final Version: String = "0.0.5";

	public static final ColorBg = 0x1a1212;

	public static final RegionSize: Point2i = [200, 125];

	public static final RerollFrameDelay: Float = .05;

	public static final MaxLocations = 6;
}
