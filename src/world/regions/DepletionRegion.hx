package world.regions;

/**
	Define a depletion region.
	This region requires the player to send die to decrease the value.
	When it hit 0, the reward is given to the player.
**/
class DepletionRegion extends Region {
	public function new(name: String, description: String, requiredValue: Int) {
		super(name, description, requiredValue);
	}
}
