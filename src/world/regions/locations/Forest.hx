package world.regions.locations;

class Forest extends SimpleLocation {
	public function new(world: World) {
		super(world, "Forest", [
			{reward: Resource(Wood), criteria: FaceValue(world.r.randomInt(3) + 2)},
			{reward: Resource(Wood), criteria: FaceValue(world.r.randomInt(3) + 2)},
			{reward: Resource(Food), criteria: LessThanEqual(2)},
			{reward: Resource(Food), criteria: LessThanEqual(2)}
		]);
	}
}
