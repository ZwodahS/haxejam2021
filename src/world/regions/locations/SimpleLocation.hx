package world.regions.locations;

typedef RewardConfiguration = {
	reward: Reward,
	criteria: MatchCriteria,
}

class SimpleLocation extends Region {
	public var dieRegions: Array<MatchDieRegion>;
	public var rewards: Array<Reward>;

	public function new(world: World, name: String, rewardsConf: Array<RewardConfiguration>) {
		super(world, name, "Forage for resources", LocationType);

		this.dieRegions = [];
		this.rewards = [];

		final layer = new h2d.Layers();

		inline function makeReward(reward: Reward, criteria: MatchCriteria) {
			final mdr = new MatchDieRegion(this, criteria);
			mdr.x = dieRegions.length * 45;
			mdr.onPlace = collect.bind(dieRegions.length);
			mdr.canRepeat = false;

			switch (reward) {
				case Resource(t):
					final text = new h2d.Text(Assets.displayFont1x);
					text.text = '${t}';
					text.textColor = Colors.White[0];
					text.putAbove(mdr, [0, 5]).setX(34, AlignCenter, mdr.x);
					layer.addChild(text);
			}

			rewards.push(reward);
			layer.addChild(mdr);
			dieRegions.push(mdr);
		}

		for (conf in rewardsConf) {
			makeReward(conf.reward, conf.criteria);
		}

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	override public function getDieRegion(bound: h2d.col.Bounds): MatchDieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];

		var stillHaveDice = false;
		for (ind => region in this.dieRegions) {
			if (region.die == null) {
				if (!region.disabled) stillHaveDice = true;
			} else {
				dice.push(region.die);
			}
		}

		if (!stillHaveDice) {
			this.world.removeLocationRegion(this);
		}

		return dice;
	}

	function collect(i: Int) {
		// get reward
		final reward = this.rewards[i];
		final region = this.dieRegions[i];
		this.world.collectReward(region, reward, region.die.faceValue);
		region.disabled = true;
	}
}
