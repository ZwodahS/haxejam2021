package world.regions.locations;

class Hill extends SimpleLocation {
	public function new(world: World) {
		super(world, "Hill", [
			{reward: Resource(Stone), criteria: FaceValue(world.r.randomInt(3) + 2)},
			{reward: Resource(Stone), criteria: FaceValue(world.r.randomInt(3) + 2)},
			{reward: Resource(Food), criteria: LessThanEqual(2)},
			{reward: Resource(Food), criteria: LessThanEqual(2)}
		]);
	}
}
