package world.regions.locations;

class MysticForest extends Region {
	/**
		Each mystic forest have full random resource + a explore deeper function.
	**/
	var depth = 0;

	public var rewardRegion: MatchDieRegion;
	public var reward: Reward;
	public var goDeeperRegion: MatchDieRegion;
	public var trainDoctorRegion: MatchDieRegion;

	public var dieRegions: Array<MatchDieRegion>;

	public function new(world: World, depth: Int = 0) {
		super(world, "Mystic Forest", "", LocationType);
		this.depth = depth;
		this.dieRegions = [];

		this.dieRegions.push(this.rewardRegion = new MatchDieRegion(this,
			FaceValue(this.world.r.randomInt(6) + 1)));

		this.rewardRegion.x = 10;
		this.rewardRegion.y = 80;
		this.rewardRegion.canRepeat = false;
		this.rewardRegion.onPlace = collect;
		this.addChild(this.rewardRegion);

		this.reward = switch (this.world.r.randomInt(3)) {
			case 0: Resource(Food);
			case 1: Resource(Wood);
			case 2: Resource(Stone);
			default: Resource(Food);
		}

		switch (reward) {
			case Resource(t):
				final text = new h2d.Text(Assets.displayFont1x);
				text.text = '${t}';
				text.textColor = Colors.White[0];
				text.putAbove(rewardRegion, [0, 5]).setX(34, AlignCenter, rewardRegion.x);
				this.addChild(text);
		}

		if (this.depth < 6 || this.world.r.randomChance(30)) {
			this.dieRegions.push(this.goDeeperRegion = new MatchDieRegion(this, FaceValue(6)));
			this.goDeeperRegion.setX(Constants.RegionSize.x, AlignCenter).setY(80);
			this.goDeeperRegion.canRepeat = false;
			this.addChild(this.goDeeperRegion);

			final text = new h2d.Text(Assets.displayFont1x);
			text.text = 'Explore';
			text.textColor = Colors.White[0];
			text.putAbove(this.goDeeperRegion, [0, 5]).setX(34, AlignCenter, goDeeperRegion.x);
			this.addChild(text);
		} else {
			this.dieRegions.push(this.trainDoctorRegion = new MatchDieRegion(this, Any));
			this.trainDoctorRegion.setX(Constants.RegionSize.x, AlignCenter).setY(80);
			this.trainDoctorRegion.canRepeat = false;
			final trainDoctorDetails = new DieDetailsWindow(world, Doctor);
			this.trainDoctorRegion.onOver = function(region: DieRegion, event: hxd.Event) {
				final bound = this.trainDoctorRegion.getBounds();
				trainDoctorDetails.putBelowBound(bound, [0, 10]);
				this.world.renderSystem.showWindow(trainDoctorDetails);
			};
			this.trainDoctorRegion.onOut = function(region: DieRegion, event: hxd.Event) {
				trainDoctorDetails.remove();
			}
			this.addChild(this.trainDoctorRegion);

			final text = new h2d.Text(Assets.displayFont1x);
			text.text = 'Study Herb';
			text.textColor = Colors.White[0];
			text.putAbove(this.trainDoctorRegion, [0, 5]).setX(34, AlignCenter, trainDoctorRegion.x);
			this.addChild(text);
		}
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		for (region in this.dieRegions) {
			dice.pushArray(region.dice);
		}

		if (goDeeperRegion == null) {
			if (this.rewardRegion.disabled) this.world.removeLocationRegion(this);
		} else if (goDeeperRegion.die != null) {
			this.world.removeLocationRegion(this);
			final newForest = new MysticForest(this.world, this.depth + 1);
			this.world.addLocationRegion(newForest);
		}

		if (this.trainDoctorRegion != null && this.trainDoctorRegion.die != null) {
			this.trainDoctorRegion.die.type = Doctor;
			this.world.removeLocationRegion(this);
		}
		return dice;
	}

	function collect() {
		this.world.collectReward(this.rewardRegion, reward, this.rewardRegion.die.faceValue);
		this.rewardRegion.disabled = true;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): MatchDieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}
}
