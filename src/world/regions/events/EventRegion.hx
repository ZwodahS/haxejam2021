package world.regions.events;

class EventRegion extends Region {
	var turnsLeft: Int;
	var turnText: h2d.Text;
	var cleared(get, never): Bool;

	public function new(world: World, name: String, description: String, turns: Int,
			type: RegionType = NegativeEventType) {
		super(world, name, description, type);
		this.turnsLeft = turns;

		this.turnText = new h2d.Text(Assets.displayFont1x);
		this.turnText.text = '(${this.turnsLeft} Turns Left)';
		this.turnText.textColor = Colors.White[0];
		this.turnText.maxWidth = Constants.RegionSize.x;
		this.turnText.textAlign = Center;
		this.turnText.y = 60;
		this.addChild(turnText);
	}

	override public function trigger() {
		final dice = collectDice();
		if (this.cleared) {
			this.world.removeEventRegion(this);
			return dice;
		}

		onTrigger();

		this.turnsLeft -= 1;
		this.turnText.text = '(${this.turnsLeft} Turns Left)';

		if (this.turnsLeft <= 0) this.onTurnFinished();
		if (this.turnsLeft <= 0 || this.cleared) this.world.removeEventRegion(this);

		return dice;
	}

	public function onTurnFinished() {}

	public function clearEvent() {}

	public function onTrigger() {}

	function get_cleared(): Bool {
		return false;
	}

	function collectDice(): Array<Die> {
		return [];
	}
}
