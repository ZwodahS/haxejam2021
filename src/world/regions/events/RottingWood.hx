package world.regions.events;

class RottingWood extends EventRegion {
	public var collectRegion: SumDieRegion;

	public function new(world: World) {
		super(world, "Rotting Wood", "Lose 10 Wood if not cleared.", 5);

		final layer = new h2d.Layers();

		this.collectRegion = new SumDieRegion(this, 15);
		this.collectRegion.reset = false;
		layer.addChild(collectRegion);
		collectRegion.onComplete = clearEvent;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	override public function collectDice() {
		final dice: Array<Die> = [];
		dice.pushArray(collectRegion.dice);
		return dice;
	}

	override function get_cleared(): Bool {
		return this.collectRegion.completed;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.collectRegion.intersects(bound)) return this.collectRegion;
		return null;
	}

	override public function onTurnFinished() {
		this.world.state.wood = Math.clampI(this.world.state.wood - 10, 0, null);
	}
}
