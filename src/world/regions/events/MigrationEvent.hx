package world.regions.events;

class MigrationEvent extends EventRegion {
	public var dieRegions: Array<MatchDieRegion>;

	public function new(world: World) {
		super(world, "Migration", "Accept new villagers", 4, PositiveEventType);

		this.dieRegions = [];

		final layer = new h2d.Layers();

		var ind = 0;
		inline function makeRegion(criteria: MatchCriteria) {
			final region = new MatchDieRegion(this, criteria);
			region.x = (ind % 3) * 40;
			region.y = (Std.int(ind / 3)) * 48;
			region.canRepeat = false;
			region.onPlace = function() {
				region.disabled = true;
			}
			ind += 1;
			layer.addChild(region);
			this.dieRegions.push(region);
		}

		makeRegion(FaceValue(1));
		makeRegion(FaceValue(2));
		makeRegion(FaceValue(3));
		makeRegion(FaceValue(4));
		makeRegion(FaceValue(5));
		makeRegion(FaceValue(6));

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 35;
		this.turnText.y = 70;
		this.addChild(layer);
	}

	override function collectDice(): Array<Die> {
		final dice = new Array<Die>();
		for (r in this.dieRegions) {
			for (die in r.dice) {
				dice.push(die);
				var newDie = this.world.addVillageDie(1);
				newDie.x = die.x;
				newDie.y = die.y;
				die.parent.addChild(newDie);
				newDie.location = Region(r);
				dice.push(newDie);
			}
		}
		return dice;
	}

	override function get_cleared(): Bool {
		for (r in this.dieRegions) {
			if (!r.disabled) return false;
		}
		return true;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}
}
