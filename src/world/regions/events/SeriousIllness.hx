package world.regions.events;

class SeriousIllness extends EventRegion {
	public var dieRegions: Array<MatchDieRegion>;

	public function new(world: World) {
		super(world, "Serious Illness", "You die", 5);

		this.dieRegions = [];

		final layer = new h2d.Layers();

		var ind = 0;
		inline function makeRegion(criteria: MatchCriteria) {
			final region = new MatchDieRegion(this, criteria);
			region.x = (ind % 4) * 40;
			region.y = (Std.int(ind / 4)) * 48;
			region.canRepeat = false;
			region.onPlace = function() {
				region.disabled = true;
			}
			ind += 1;
			layer.addChild(region);
			this.dieRegions.push(region);
		}

		makeRegion(FaceValue(5));
		makeRegion(FaceValue(5));
		makeRegion(FaceValue(5));

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	override function collectDice(): Array<Die> {
		final dice = new Array<Die>();
		for (r in this.dieRegions) {
			dice.pushArray(r.dice);
		}
		return dice;
	}

	override function get_cleared(): Bool {
		for (r in this.dieRegions) {
			if (!r.disabled) return false;
		}
		return true;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}

	override public function onTurnFinished() {
		this.world.gameover("illness");
	}
}
