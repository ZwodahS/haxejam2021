package world.regions;

enum MatchCriteria {
	Any;
	FaceValue(i: Int);
	LessThanEqual(i: Int);
	MoreThanEqual(i: Int);
	Odd;
	Even;
}
