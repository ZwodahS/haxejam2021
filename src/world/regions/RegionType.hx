package world.regions;

enum RegionType {
	ActionType;
	LocationType;
	BuildingType;
	PositiveEventType;
	NegativeEventType;
	BuildingConstructionType;
}
