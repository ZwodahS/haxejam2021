package world.regions;

/**
	Defines a region that a die can be drag to.

	Note that this does not define the "square" that the die can be drag to.
	Instead this is the whole region containing the text etc.
**/
class Region extends h2d.Layers {
	public static final MoveSpeed: Point2f = [1200, 200];

	public var world: World;
	public var regionName: String;
	public var description: String;

	public var targetX: Float = 0;
	public var targetY: Float = 0;

	public function new(world: World, name: String, description: String, type: RegionType) {
		super();
		this.world = world;
		this.regionName = name;
		this.description = description;

		this.addChild(Assets.packed.assets.get("region:background").getBitmap());
		switch (type) {
			case ActionType:
				this.addChild(Assets.packed.assets.get("region:action").getBitmap());
			case LocationType:
				this.addChild(Assets.packed.assets.get("region:location").getBitmap());
			case BuildingType:
				this.addChild(Assets.packed.assets.get("region:building").getBitmap());
			case BuildingConstructionType:
				this.addChild(Assets.packed.assets.get("region:building").getBitmap());
			case PositiveEventType:
				this.addChild(Assets.packed.assets.get("region:goodevent").getBitmap());
			case NegativeEventType:
				this.addChild(Assets.packed.assets.get("region:badevent").getBitmap());
		}

		final title = new h2d.Text(Assets.displayFont2x);
		title.maxWidth = Constants.RegionSize.x;
		title.textColor = Colors.White[0];
		title.textAlign = Center;
		title.y = 1;
		title.text = name;
		this.addChild(title);

		final descriptionText = new h2d.HtmlText(Assets.displayFont1x);
		descriptionText.maxWidth = Constants.RegionSize.x - 10;
		descriptionText.textAlign = Center;
		descriptionText.textColor = Colors.White[0];
		descriptionText.setX(5).setY(title.y + 20);
		descriptionText.text = this.description.replace("\n", "<br/>");
		this.addChild(descriptionText);
	}

	public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		return null;
	}

	public function trigger(): Array<Die> {
		return [];
	}

	public function update(dt: Float) {
		if (this.targetX > this.x) {
			this.x += dt * MoveSpeed.x;
			if (this.x > this.targetX) this.x = this.targetX;
		} else if (this.targetX < this.x) {
			this.x -= dt * MoveSpeed.x;
			if (this.x < this.targetX) this.x = this.targetX;
		}

		if (this.targetY > this.y) {
			this.y += dt * MoveSpeed.y;
			if (this.y > this.targetY) this.y = this.targetY;
		} else if (this.targetY < this.y) {
			this.y -= dt * MoveSpeed.y;
			if (this.y < this.targetY) this.y = this.targetY;
		}
	}

	public function canPlaceDie(die: Die, region: DieRegion) {
		return true;
	}
}
