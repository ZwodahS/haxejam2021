package world.regions;

class SumDieRegion extends DieRegion {
	public var required(default, null): Int;
	public var leftover(get, never): Int;
	public var reset: Bool = true;
	public var completed(get, never): Bool;

	var current: Int = 0;
	var text: h2d.Text;

	public function new(region: Region, required: Int) {
		super(region);
		this.current = 0;
		this.required = required;

		render();
	}

	public function get_leftover(): Int {
		return this.required - this.current;
	}

	public function get_completed(): Bool {
		return leftover <= 0;
	}

	function render() {
		final DieFaceColor = 0xff9d6d51;
		if (this.region == null) {
			this.addChild(Assets.packed.assets.get("dieborder:tutorial").getBitmap(0));
		} else {
			this.addChild(Assets.packed.assets.get("dieborder:lightbrown").getBitmap(0));
		}
		this.text = new h2d.Text(Assets.displayFont3x);
		text.textColor = DieFaceColor;
		text.text = '${this.leftover}';
		text.setX(34, AlignCenter).setY(34, AlignCenter);
		this.addChild(text);
	}

	override public function canPlace(die: Die) {
		if (!this.region.canPlaceDie(die, this)) return false;
		if (!this.reset && this.current >= this.required) return false;
		return true;
	}

	override public function place(die: Die, onFinish: Void->Void) {
		this.current += getFaceValue(die);
		this.dice.push(die);
		die.location = Region(this);

		if (this.current >= this.required) {
			if (reset) {
				this.current = 0;
			} else {
				this.disabled = true;
			}
			onComplete();
		}
		update();
		onFinish();
	}

	public function update() {
		text.text = '${this.leftover}';
		text.setX(34, AlignCenter).setY(34, AlignCenter);
	}

	dynamic public function onComplete() {}

	dynamic public function getFaceValue(die: Die): Int {
		return die.faceValue;
	}
}
