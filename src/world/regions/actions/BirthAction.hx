package world.regions.actions;

class BirthAction extends Region {
	public static final BirthCost: Int = 5;

	public var birthRegion: MatchDieRegion;

	public function new(world: World) {
		super(world, "Birth", 'Create a new villager at end of year.\nCost: ${BirthCost} Food', ActionType);

		final layer = new h2d.Layers();
		this.birthRegion = new MatchDieRegion(this, FaceValue(1));
		this.birthRegion.canRepeat = false;
		this.birthRegion.lock = true;
		layer.addChild(birthRegion);

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;

		this.addChild(layer);
	}

	override public function trigger(): Array<Die> {
		if (this.birthRegion.dice.length == 0) return [];
		final die = this.birthRegion.die;
		final dice = [die];

		if (this.world.state.food >= BirthCost) {
			var newDie = this.world.addVillageDie(1);
			// set the new die location so we can animate back
			newDie.x = die.x;
			newDie.y = die.y;
			die.parent.addChild(newDie);

			dice.push(newDie);
			newDie.location = Region(birthRegion);

			this.world.state.food -= BirthCost;
		}
		return dice;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.birthRegion.intersects(bound)) return this.birthRegion;
		return null;
	}
}
