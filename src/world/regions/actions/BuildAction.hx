package world.regions.actions;

class BuildAction extends Region {
	public var buildFarm: MatchDieRegion;
	public var buildToolShed: MatchDieRegion;

	public var dieRegions: Array<MatchDieRegion>;

	public function new(world: World) {
		super(world, "Build", "", ActionType);
		this.dieRegions = [];

		final layer = new h2d.Layers();

		var ind = 0;
		inline function makeRegion(criteria: MatchCriteria, title: String) {
			final region = new MatchDieRegion(this, criteria);
			region.x = (ind % 3) * 60;
			region.y = (Std.int(ind / 3)) * 48;
			ind += 1;
			layer.addChild(region);
			this.dieRegions.push(region);

			final text = new h2d.Text(Assets.displayFont1x);
			text.text = '${title}';
			text.textColor = Colors.White[0];
			text.putAbove(region, [0, 5]).setX(34, AlignCenter, region.x);
			layer.addChild(text);

			return region;
		}

		this.buildFarm = makeRegion(FaceValue(4), "Farm");
		this.buildFarm.onPlace = build.bind("farm");
		this.buildFarm.canRepeat = false;

		this.buildToolShed = makeRegion(FaceValue(4), "ToolShed");
		this.buildToolShed.onPlace = build.bind("toolshed");
		this.buildToolShed.canRepeat = false;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	function build(type: String) {
		switch (type) {
			case "farm":
				final farm = new Farm(this.world);
				final construction = new ConstructionRegion(this.world, farm, [
					{resourceType: Wood, resourceAmt: 1, criteria: LessThanEqual(4)},
					{resourceType: Wood, resourceAmt: 1, criteria: LessThanEqual(4)},
					{resourceType: Wood, resourceAmt: 2, criteria: MoreThanEqual(4)},
					{resourceType: Wood, resourceAmt: 2, criteria: MoreThanEqual(4)},
				]);
				this.world.addBuildingRegion(construction);
			case "toolshed":
				final toolshed = new ToolShed(this.world);
				final construction = new ConstructionRegion(this.world, toolshed, [
					{resourceType: Wood, resourceAmt: 1, criteria: LessThanEqual(4)},
					{resourceType: Wood, resourceAmt: 1, criteria: LessThanEqual(4)},
					{resourceType: Wood, resourceAmt: 1, criteria: MoreThanEqual(4)},
					{resourceType: Stone, resourceAmt: 3, criteria: MoreThanEqual(4)},
				]);
				this.world.addBuildingRegion(construction);
			default:
		}
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		for (r in this.dieRegions) {
			dice.pushArray(r.dice);
		}
		return dice;
	}
}
