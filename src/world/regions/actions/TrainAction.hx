package world.regions.actions;

class TrainAction extends Region {
	public var trainFarmer: MatchDieRegion;
	public var trainBuilder: MatchDieRegion;
	public var trainExplorer: MatchDieRegion;
	public var trainForager: MatchDieRegion;

	// public var trainWoodcutter: MatchDieRegion;

	public function new(world: World) {
		super(world, "Train", "", ActionType);

		final layer = new h2d.Layers();

		var ind = 0;
		inline function makeTrain(criteria: MatchCriteria, title: String) {
			final region = new MatchDieRegion(this, criteria);
			region.canRepeat = false;
			final text = new h2d.Text(Assets.displayFont0x);
			text.text = '${title}';
			text.textColor = Colors.White[0];
			region.x = (ind % 3) * 60;
			region.y = (Std.int(ind / 3)) * 48;
			text.putAbove(region, [0, 4]).setX(34, AlignCenter, region.x);
			ind += 1;
			layer.addChild(region);
			layer.addChild(text);
			return region;
		}
		this.trainFarmer = makeTrain(Any, "Farmer");
		final trainFarmerDetails = new DieDetailsWindow(world, Farmer);
		this.trainFarmer.onOver = function(region: DieRegion, event: hxd.Event) {
			final bound = this.trainFarmer.getBounds();
			trainFarmerDetails.putBelowBound(bound, [0, 10]);
			this.world.renderSystem.showWindow(trainFarmerDetails);
		};
		this.trainFarmer.onOut = function(region: DieRegion, event: hxd.Event) {
			trainFarmerDetails.remove();
		};

		this.trainBuilder = makeTrain(Any, "Builder");
		final trainBuilderDetails = new DieDetailsWindow(world, Builder);
		this.trainBuilder.onOver = function(region: DieRegion, event: hxd.Event) {
			final bound = this.trainBuilder.getBounds();
			trainBuilderDetails.putBelowBound(bound, [0, 10]);
			this.world.renderSystem.showWindow(trainBuilderDetails);
		};
		this.trainBuilder.onOut = function(region: DieRegion, event: hxd.Event) {
			trainBuilderDetails.remove();
		};

		this.trainExplorer = makeTrain(Any, "Explorer");
		final trainExplorerDetails = new DieDetailsWindow(world, Explorer);
		this.trainExplorer.onOver = function(region: DieRegion, event: hxd.Event) {
			final bound = this.trainExplorer.getBounds();
			trainExplorerDetails.putBelowBound(bound, [0, 10]);
			this.world.renderSystem.showWindow(trainExplorerDetails);
		};
		this.trainExplorer.onOut = function(region: DieRegion, event: hxd.Event) {
			trainExplorerDetails.remove();
		};

		this.trainForager = makeTrain(Any, "Forager");
		final trainForagerDetails = new DieDetailsWindow(world, Forager);
		this.trainForager.onOver = function(region: DieRegion, event: hxd.Event) {
			final bound = this.trainForager.getBounds();
			trainForagerDetails.putBelowBound(bound, [0, 10]);
			this.world.renderSystem.showWindow(trainForagerDetails);
		};
		this.trainForager.onOut = function(region: DieRegion, event: hxd.Event) {
			trainForagerDetails.remove();
		};

		/**
			this.trainWoodcutter = makeTrain(Any, "Woodcutter");
			final trainWoodcutterDetails = new DieDetailsWindow(world, Woodcutter);
			this.trainWoodcutter.onOver = function(region: DieRegion, event: hxd.Event) {
				final bound = this.trainWoodcutter.getBounds();
				trainWoodcutterDetails.putBelowBound(bound, [0, 10]);
				this.world.renderSystem.showWindow(trainWoodcutterDetails);
			};
			this.trainWoodcutter.onOut = function(region: DieRegion, event: hxd.Event) {
				trainWoodcutterDetails.remove();
			};
		**/

		layer.setX(20);

		layer.y = 35;
		this.addChild(layer);
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.trainFarmer.intersects(bound)) return this.trainFarmer;
		if (this.trainBuilder.intersects(bound)) return this.trainBuilder;
		if (this.trainExplorer.intersects(bound)) return this.trainExplorer;
		if (this.trainForager.intersects(bound)) return this.trainForager;
		// if (this.trainWoodcutter.intersects(bound)) return this.trainWoodcutter;
		return null;
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		for (die in trainFarmer.dice) {
			die.type = Farmer;
			dice.push(die);
		}
		for (die in trainBuilder.dice) {
			die.type = Builder;
			dice.push(die);
		}
		for (die in trainExplorer.dice) {
			die.type = Explorer;
			dice.push(die);
		}
		for (die in trainForager.dice) {
			die.type = Forager;
			dice.push(die);
		}
		/**
			for (die in trainWoodcutter.dice) {
				die.type = Woodcutter;
				dice.push(die);
			}
		**/
		return dice;
	}
}
