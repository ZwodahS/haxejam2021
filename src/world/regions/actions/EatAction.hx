package world.regions.actions;

class EatAction extends Region {
	public var eatRegion: MatchDieRegion;

	public function new(world: World) {
		super(world, "Eat", 'Reroll die for 1 food', ActionType);

		final layer = new h2d.Layers();
		this.eatRegion = new MatchDieRegion(this, Any);
		layer.addChild(this.eatRegion);
		this.eatRegion.onPlace = eat;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	function eat() {
		var die = this.eatRegion.die;
		if (die == null) return; // shouldn't happen
		this.world.returnDiceToVillage([die]);
		this.world.state.food -= 1;
		die.reroll(this.world.r);
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.eatRegion.intersects(bound)) return this.eatRegion;
		return null;
	}

	override public function canPlaceDie(die: Die, region: DieRegion): Bool {
		if (this.world.state.food == 0) return false;
		return true;
	}
}
