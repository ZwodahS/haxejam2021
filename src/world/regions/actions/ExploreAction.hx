package world.regions.actions;

class ExploreAction extends Region {
	public var exploreForestRegion: MatchDieRegion;
	public var exploreHillRegion: MatchDieRegion;

	public var dieRegions: Array<MatchDieRegion>;

	public function new(world: World) {
		super(world, "Explore", 'Explore a new Location', ActionType);
		this.dieRegions = [];

		final layer = new h2d.Layers();

		var ind = 0;
		inline function makeRegion(criteria: MatchCriteria, title: String) {
			final region = new MatchDieRegion(this, criteria);
			region.x = (ind % 3) * 60;
			region.y = (Std.int(ind / 3)) * 48;
			ind += 1;
			layer.addChild(region);
			this.dieRegions.push(region);

			final text = new h2d.Text(Assets.displayFont1x);
			text.text = '${title}';
			text.textColor = Colors.White[0];
			text.putAbove(region, [0, 5]).setX(34, AlignCenter, region.x);
			layer.addChild(text);

			return region;
		}

		this.dieRegions.push(this.exploreForestRegion = makeRegion(FaceValue(6), "Forest"));
		this.exploreForestRegion.onPlace = explore.bind("forest");
		this.exploreForestRegion.canRepeat = false;

		this.dieRegions.push(this.exploreHillRegion = makeRegion(FaceValue(6), "Hill"));
		this.exploreHillRegion.onPlace = explore.bind("hill");
		this.exploreHillRegion.canRepeat = false;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		for (r in this.dieRegions) {
			dice.pushArray(r.dice);
		}
		return dice;
	}

	function explore(l: String = null) {
		var location: Region = null;
		if (l != null) {
			switch (l) {
				case "forest":
					final c = this.world.r.randomChance(60);
					if (this.world.state.specialLocations["mystic"] != null || this.world.state.turn < 5 || c) {
						location = new Forest(this.world);
					} else {
						location = new MysticForest(this.world);
					}
				case "hill":
					location = new Hill(this.world);
			}
		}
		if (location == null) {
			location = world.getRandomLocation();
		}
		if (location != null) world.addLocationRegion(location);
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}

	override public function canPlaceDie(die: Die, region: DieRegion): Bool {
		if (this.world.state.locations.length >= Constants.MaxLocations) return false;
		return true;
	}
}
