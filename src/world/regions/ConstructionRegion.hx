package world.regions;

typedef ConstructionRequirement = {
	resourceType: ResourceType,
	resourceAmt: Int,
	criteria: MatchCriteria,
}

class ConstructionRegion extends Region {
	var dieRegions: Array<DieRegion>;
	var building: Region;
	var requirements: Array<ConstructionRequirement>;

	public function new(world: World, building: Region, requirements: Array<ConstructionRequirement>) {
		super(world, "Construction", 'Finish ${building.regionName} construction', BuildingConstructionType);
		this.building = building;
		this.requirements = requirements;
		this.dieRegions = [];

		final layer = new h2d.Layers();

		for (ind => requirement in this.requirements) {
			final mdr = new MatchDieRegion(this, requirement.criteria);
			mdr.x = ind * 45;
			mdr.onPlace = build.bind(ind);
			mdr.canRepeat = false;
			layer.addChild(mdr);
			dieRegions.push(mdr);
			final text = new h2d.Text(Assets.displayFont1x);
			text.text = '${requirement.resourceAmt}\n${requirement.resourceType}';
			text.textColor = Colors.White[0];
			text.putAbove(mdr, [0, 5]).setX(mdr.x);
			text.maxWidth = 34;
			text.textAlign = Center;
			layer.addChild(text);
		}

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);
	}

	function build(i: Int) {
		this.dieRegions[i].disabled = true;
		this.world.state.depleteResource(requirements[i].resourceType, requirements[i].resourceAmt);
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		for (r in this.dieRegions) {
			if (r.intersects(bound)) return r;
		}
		return null;
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		for (region in this.dieRegions) {
			dice.pushArray(region.dice);
		}

		var completed = true;
		for (r in this.dieRegions) {
			if (!r.disabled) completed = false;
		}

		if (completed) {
			this.world.addBuildingRegion(this.building);
			this.world.removeBuildingRegion(this);
		}

		return dice;
	}

	override public function canPlaceDie(die: Die, region: DieRegion): Bool {
		if (die.type == Explorer) return false;
		final ind = this.dieRegions.indexOf(region);
		if (ind == -1) return false;
		if (region.disabled) return false;
		final requirement = this.requirements[ind];
		if (!this.world.state.hasResource(requirement.resourceType, requirement.resourceAmt)) return false;
		return true;
	}
}
