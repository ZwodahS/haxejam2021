package world.regions;

enum abstract ResourceType(String) from String to String {
	public var Food = "Food";
	public var Wood = "Wood";
	public var Stone = "Stone";
}
