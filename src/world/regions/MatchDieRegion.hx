package world.regions;

class MatchDieRegion extends DieRegion {
	public var criteria(default, null): MatchCriteria;

	public var canRepeat: Bool = true;
	public var lock: Bool = true;

	var overrideText: String = null;

	public function new(region: Region, criteria: MatchCriteria, overrideText: String = null) {
		super(region);
		this.criteria = criteria;
		this.overrideText = overrideText;
		render();
	}

	function render() {
		final DieFaceColor = 0xff9d6d51;
		if (this.region == null) {
			this.addChild(Assets.packed.assets.get("dieborder:tutorial").getBitmap(0));
		} else {
			this.addChild(Assets.packed.assets.get("dieborder:lightbrown").getBitmap(0));
		}
		switch (this.criteria) {
			case Any:
				var text: h2d.Text = null;
				if (overrideText != null) {
					text = new h2d.Text(Assets.displayFont2x);
					text.text = '${this.overrideText}';
				} else {
					text = new h2d.Text(Assets.displayFont1x);
					text.text = "Any";
				}
				text.textColor = DieFaceColor;
				text.setX(34, AlignCenter).setY(34, AlignCenter);
				this.addChild(text);
			case FaceValue(i):
				this.addChild(Assets.packed.assets.get("dieborder:lightbrown").getBitmap(i));
			case LessThanEqual(i):
				final text = new h2d.Text(Assets.displayFont2x);
				text.textColor = DieFaceColor;
				text.text = '<=${i}';
				text.setX(34, AlignCenter).setY(34, AlignCenter);
				this.addChild(text);
			case MoreThanEqual(i):
				final text = new h2d.Text(Assets.displayFont2x);
				text.textColor = DieFaceColor;
				text.text = '>=${i}';
				text.setX(34, AlignCenter).setY(34, AlignCenter);
				this.addChild(text);
			case Odd:
				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = DieFaceColor;
				text.text = "Odd";
				text.setX(34, AlignCenter).setY(34, AlignCenter);
				this.addChild(text);
			case Even:
				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = DieFaceColor;
				text.text = "Even";
				text.setX(34, AlignCenter).setY(34, AlignCenter);
				this.addChild(text);
		}
	}

	override public function canPlace(die: Die): Bool {
		if (!this.region.canPlaceDie(die, this)) return false;
		if (!die.match(this.criteria)) return false;
		if (this.canRepeat) return true;
		return this.dice.length == 0;
	}

	override public function place(die: Die, onFinish: Void->Void) {
		die.location = Region(this);
		this.dice.push(die);
		if (this.lock && !this.canRepeat) {
			this.addChild(die);
			die.x = 2;
			die.y = 2;
		}
		this.onPlace();
		onFinish();
	}

	dynamic public function onPlace() {}
}
