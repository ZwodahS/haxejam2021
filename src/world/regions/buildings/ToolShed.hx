package world.regions.buildings;

class ToolShed extends Region {
	public static final ToolShedUses = 10;

	public var upgradeRegion: MatchDieRegion;
	public var downgradeRegion: MatchDieRegion;

	public var usesText: h2d.Text;
	public var uses(default, set): Int = 0;

	public function new(world: World) {
		super(world, "Tool Shed", "Change the die value of normal villager", BuildingType);

		final layer = new h2d.Layers();
		var ind = 0;
		inline function makeRegion(criteria: MatchCriteria, title: String) {
			final region = new MatchDieRegion(this, criteria, title);
			region.x = (ind % 3) * 60;
			region.y = (Std.int(ind / 3)) * 48;
			ind += 1;
			layer.addChild(region);
			return region;
		}

		this.downgradeRegion = makeRegion(Any, "-1");
		this.downgradeRegion.onPlace = downgrade;

		this.upgradeRegion = makeRegion(Any, "+1");
		this.upgradeRegion.onPlace = upgrade;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);

		this.usesText = new h2d.Text(Assets.displayFont1x);
		usesText.text = '${this.uses} left';
		usesText.textColor = Colors.White[0];
		usesText.setY(67);
		usesText.maxWidth = Constants.RegionSize.x;
		usesText.textAlign = Center;
		this.addChild(usesText);

		this.uses = ToolShedUses;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.upgradeRegion.intersects(bound)) return this.upgradeRegion;
		if (this.downgradeRegion.intersects(bound)) return this.downgradeRegion;
		return null;
	}

	override public function canPlaceDie(die: Die, dieRegion: DieRegion): Bool {
		if (die.type != Normal) return false;
		if (dieRegion == upgradeRegion) {
			if (die.faceValue == 6) return false;
			return true;
		}
		if (dieRegion == downgradeRegion) {
			if (die.faceValue == 1) return false;
			return true;
		}
		return false;
	}

	public function set_uses(v: Int): Int {
		this.uses = v;
		this.usesText.text = '${this.uses}/${ToolShedUses} uses left';
		return this.uses;
	}

	function upgrade() {
		final die = this.upgradeRegion.die;
		if (die == null) return;
		Assert.assert(die.type == Normal);
		this.world.returnDiceToVillage([die]);
		die.faceIndex += 1;
		deplete();
	}

	function downgrade() {
		final die = this.downgradeRegion.die;
		if (die == null) return;
		Assert.assert(die.type == Normal);
		this.world.returnDiceToVillage([die]);
		die.faceIndex -= 1;
		deplete();
	}

	function deplete() {
		this.uses -= 1;
		if (this.uses <= 0) this.world.removeBuildingRegion(this);
	}
}
