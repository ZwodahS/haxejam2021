package world.regions.buildings;

class Farm extends Region {
	public static final FarmRequired = 15;
	public static final FarmCollectAmt = 10;
	public static final FarmUses = 10;

	// the region used to collect resources
	public var collectRegion: SumDieRegion;

	public var usesText: h2d.Text;
	public var uses(default, set): Int = 0;

	public function new(world: World) {
		// @formatter:off
		super(world, "Farm",
			'Food +${FarmCollectAmt}. Normal and Farmer villagers only.',
			BuildingType
		);

		final layer = new h2d.Layers();

		this.collectRegion = new SumDieRegion(this, FarmRequired);
		this.collectRegion.getFaceValue = getFaceValue;
		layer.addChild(collectRegion);
		collectRegion.onComplete = collect;

		layer.setX(Constants.RegionSize.x, AlignCenter);
		layer.y = 80;
		this.addChild(layer);

		this.usesText = new h2d.Text(Assets.displayFont1x);
		usesText.text = '${this.uses} left';
		usesText.textColor = Colors.White[0];
		usesText.setY(67);
		usesText.maxWidth = Constants.RegionSize.x;
		usesText.textAlign = Center;
		this.addChild(usesText);

		this.uses = FarmUses;
	}

	override public function getDieRegion(bound: h2d.col.Bounds): DieRegion {
		if (this.collectRegion.intersects(bound)) return this.collectRegion;
		return null;
	}

	function collect() {
		this.world.collectReward(collectRegion, Resource(Food), FarmCollectAmt);
		this.uses -= 1;
		if (this.uses <= 0) this.collectRegion.disabled = true;
	}

	public function set_uses(v: Int): Int {
		this.uses = v;
		this.usesText.text = '${this.uses}/${FarmUses} uses left';
		this.collectRegion.disabled = this.uses <= 0;
		return this.uses;
	}

	override public function trigger(): Array<Die> {
		final dice: Array<Die> = [];
		dice.pushArray(this.collectRegion.dice);
		if (this.uses == 0) this.world.removeBuildingRegion(this);
		return dice;
	}

	override public function canPlaceDie(die: Die, dieRegion: DieRegion): Bool {
		if (this.uses == 0) return false;
		return die.type == Normal || die.type == Farmer;
	}

	function getFaceValue(die: Die): Int {
		if (die.type == Farmer) return die.faceValue * 2;
		return die.faceValue;
	}
}
