package world.regions;

/**
	Defines the die region
**/
class DieRegion extends h2d.Object {
	public var region: Region;
	public var dice: Array<Die>;
	public var disabled(default, set): Bool = false;
	public var die(get, never): Die;

	public function new(region: Region) {
		super();
		this.region = region;
		this.dice = [];

		final interactive = new h2d.Interactive(30, 30, this);

		interactive.onOver = function(e: hxd.Event) {
			onOver(this, e);
		}
		interactive.onOut = function(e: hxd.Event) {
			onOut(this, e);
		}
		interactive.onClick = function(e: hxd.Event) {
			if (e.button == 1) {
				onRightClick(this, e);
			} else {
				onLeftClick(this, e);
			}
		}
		interactive.onKeyDown = function(e: hxd.Event) {
			onKeyDown(this, e);
		}
		interactive.onPush = function(e: hxd.Event) {
			onPush(this, e);
		}
		interactive.onRelease = function(e: hxd.Event) {
			onRelease(this, e);
		}
		interactive.enableRightButton = true;
		interactive.cursor = Default;
		interactive.propagateEvents = false;
	}

	public function canPlace(die: Die): Bool {
		return false;
	}

	public function place(die: Die, onFinish: Void->Void) {}

	public function intersects(bound: h2d.col.Bounds): Bool {
		if (this.disabled) return false;
		final b = this.getBounds();
		return b.intersects(bound);
	}

	public function set_disabled(b: Bool): Bool {
		this.disabled = b;
		if (this.disabled) {
			this.alpha = .5;
		} else {
			this.alpha = 1;
		}
		return this.disabled;
	}

	public function get_die(): Die {
		return this.dice.length == 0 ? null : this.dice[0];
	}

	public function removeDie(d: Die) {
		this.dice.remove(d);
	}

	dynamic public function onOver(region: DieRegion, event: hxd.Event) {}

	dynamic public function onOut(region: DieRegion, event: hxd.Event) {}

	dynamic public function onLeftClick(region: DieRegion, event: hxd.Event) {}

	dynamic public function onRightClick(region: DieRegion, event: hxd.Event) {}

	dynamic public function onKeyDown(region: DieRegion, event: hxd.Event) {}

	dynamic public function onPush(region: DieRegion, event: hxd.Event) {}

	dynamic public function onRelease(region: DieRegion, event: hxd.Event) {}
}
