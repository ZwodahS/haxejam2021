package world;

class WorldState {
	public var food: Int;
	public var wood: Int;
	public var stone: Int;

	public var villagers: Array<Die>;
	public var actions: Array<Region>;
	public var locations: Array<Region>;
	public var buildings: Array<Region>;
	public var events: Array<Region>;
	public var turn: Int;

	public var specialLocations: Map<String, Region>;

	public var peakPopulation: Int = 0;
	public var death: Int = 0;

	public function new() {
		this.food = 45;
		this.wood = 0;
		this.stone = 0;
		this.turn = 0;
		this.villagers = [];
		this.actions = [];
		this.locations = [];
		this.buildings = [];
		this.events = [];
		this.specialLocations = new Map<String, Region>();
	}

	public function toString() {
		final str: Array<String> = [];
		str.push('food: ${this.food}');
		str.push('wood: ${this.wood}');
		return str.join("\n");
	}

	public function hasResource(type: ResourceType, amt: Int): Bool {
		switch (type) {
			case Food:
				return this.food >= amt;
			case Wood:
				return this.wood >= amt;
			case Stone:
				return this.stone >= amt;
		}
		return false;
	}

	public function depleteResource(type: ResourceType, amt: Int): Bool {
		switch (type) {
			case Food:
				this.food -= amt;
				return true;
			case Wood:
				this.wood -= amt;
				return true;
			case Stone:
				this.stone -= amt;
				return true;
		}
		return false;
	}
}

class World {
	public var r: hxd.Rand;

	public var state: WorldState;
	public var gameState: GameState = SettingUp;

	public var animator: zf.animations.Animator;

	public var systems: Array<System>;
	public var tutorialSystem: TutorialSystem;
	public var renderSystem: RenderSystem;
	public var cleanupSystem: CleanupSystem;
	public var eventSystem: EventSystem;

	public var dispatcher: MessageDispatcher;
	public var rules: Rules;

	public function new() {
		this.dispatcher = new MessageDispatcher();
		this.animator = new zf.animations.Animator();
		this.systems = [];
		this.rules = Rules.init();

		this.addSystem(this.renderSystem = new RenderSystem());
		this.addSystem(this.tutorialSystem = new TutorialSystem(this.renderSystem.tutorialLayers));
		this.addSystem(this.cleanupSystem = new CleanupSystem());
		this.addSystem(this.eventSystem = new EventSystem());

		restartGame();
	}

	public function restartGame() {
		this.state = new WorldState();
		this.r = new hxd.Rand(Random.int(0, zf.Constants.SeedMax));
		this.renderSystem.onGameRestart();

		for (i in 0...3) {
			addVillageDie((i % 6));
		}
		this.state.villagers[0].faceIndex = 0;
		this.state.villagers[1].faceIndex = 1;
		this.state.villagers[2].faceIndex = 5;
		addActionRegion(new BirthAction(this), false);
		addActionRegion(new ExploreAction(this), false);

		var farm: Farm = null;
		addBuildingRegion(farm = new Farm(this), false);
		farm.uses = 1;

		var toolshed: ToolShed = null;
		addBuildingRegion(toolshed = new ToolShed(this), false);
		toolshed.uses = 1;

		startTurn(false);
	}

	public function addSystem(system: System) {
		this.systems.push(system);
		system.init(this);
	}

	public function endTurn() {
		if (this.gameState != Idle) return;
		this.gameState = EndingTurn;
		this.cleanupSystem.startCleanup(function() {
			startUpkeep();
		});
	}

	function startUpkeep() {
		this.state.peakPopulation = Math.clampI(this.state.villagers.length, this.state.peakPopulation, null);
		final consuming: Array<Die> = [];
		consuming.pushArray(this.state.villagers);
		consuming.shuffle(this.r);

		final consumed: Array<Die> = [];
		final death: Array<Die> = [];
		if (this.state.food >= consuming.length) {
			consumed.pushArray(consuming);
		} else {
			var ind = 0;
			while (ind < this.state.food) {
				consumed.push(consuming[ind++]);
			}
			while (ind < consuming.length) {
				death.push(consuming[ind++]);
			}
		}

		killVillagers(death, function() {
			consumeFoods(consumed, function() {
				this.startTurn();
			});
		});
	}

	function startTurn(rerollDice = true) {
		if (this.state.villagers.length == 0) {
			gameover();
		} else {
			this.gameState = Idle;
		}
		if (rerollDice) this.rerollAll();
		this.state.turn += 1;
		this.eventSystem.onTurnStart();
		this.tutorialSystem.onTurnStart();
	}

	public function gameover(reason: String = null) {
		this.gameState = Gameover;
		this.renderSystem.showGameover();
	}

	public function update(dt: Float) {
		this.animator.update(dt);
		for (system in this.systems) {
			system.update(dt);
		}
		for (d in this.state.villagers) {
			d.update(this.r, dt);
		}
	}

	public function rerollAll() {
		for (d in this.state.villagers) {
			if (d.location == InVillage) d.reroll(this.r);
		}
	}

	public function addActionRegion(region: Region, animate: Bool = true) {
		this.renderSystem.addActionRegion(region, animate);
		this.state.actions.push(region);
	}

	public function addLocationRegion(region: Region, animate: Bool = true) {
		this.renderSystem.onLocationAdded(region, animate);
		this.state.locations.push(region);
		// refactor this later
		if (Std.isOfType(region, MysticForest)) {
			this.state.specialLocations["mystic"] = region;
		}
	}

	public function removeLocationRegion(region: Region) {
		this.state.locations.remove(region);
		this.renderSystem.onLocationRemoved(region);

		if (Std.isOfType(region, MysticForest)) {
			this.state.specialLocations.remove("mystic");
		}
	}

	public function addBuildingRegion(region: Region, animate: Bool = true) {
		this.renderSystem.onBuildingAdded(region, animate);
		this.state.buildings.push(region);
	}

	public function addEventRegion(region: Region, animate: Bool = true) {
		if (this.state.events.length >= Constants.MaxLocations) return;
		this.renderSystem.onEventAdded(region, animate);
		this.state.events.push(region);
	}

	public function removeEventRegion(region: Region) {
		this.state.events.remove(region);
		this.renderSystem.onEventRemoved(region);
	}

	public function removeBuildingRegion(region: Region) {
		this.state.buildings.remove(region);
		this.renderSystem.onBuildingRemoved(region);
	}

	function consumeFoods(dice: Array<Die>, onFinish: Void->Void) {
		this.state.food -= dice.length;
		this.renderSystem.animateFoodConsumed(dice, onFinish);
	}

	public function killVillagers(dice: Array<Die>, onFinish: Void->Void = null) {
		for (die in dice) {
			this.state.villagers.remove(die);
		}
		this.state.death += dice.length;
		this.renderSystem.animateDeaths(dice, onFinish);
	}

	public function addVillageDie(faceValue: Int): Die {
		return this.renderSystem.addVillageDie(faceValue);
	}

	public function returnDiceToVillage(dice: Array<Die>, onFinish: Void->Void = null) {
		this.renderSystem.returnDiceToVillage(dice, onFinish);
	}

	public function collectReward(dieRegion: DieRegion, reward: Reward, amount: Int) {
		switch (reward) {
			case Resource(t):
				switch (t) {
					case Food:
						this.state.food += amount;
					case Wood:
						this.state.wood += amount;
					case Stone:
						this.state.stone += amount;
				}
			default:
		}
		this.renderSystem.onRewardCollected(dieRegion, reward, amount);
	}

	public function getRandomLocation(): Region {
		final randomInt = this.r.randomInt(100);
		if (randomInt < 50) {
			return new Forest(this);
		} else {
			return new Hill(this);
		}
	}
}
