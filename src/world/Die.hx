package world;

enum DieLocation {
	Void;
	InVillage;
	Region(r: DieRegion);
}

enum DieType {
	Normal;
	Builder;
	Farmer;
	Explorer;
	Forager;
	Woodcutter;
	Doctor;
}

class Die extends h2d.Object {
	public static final MoveSpeed: Point2f = [200, 200];

	public static final NormalDice = [1, 2, 3, 4, 5, 6];
	public static final FarmerDice = [2, 2, 3, 3, 4, 4];
	public static final BuilderDice = [3, 4, 4, 4, 4, 5];
	public static final ExplorerDice = [2, 2, 3, 6, 6, 6];
	public static final ForagerDice = [2, 2, 2, 2, 2, 6];
	public static final WoodcutterDice = [2, 2, 2, 3, 3, 3];
	public static final DoctorDice = [2, 2, 5, 5, 5, 5];

	public var face: h2d.Anim;

	public var faces(default, null): Array<Int>;
	public var faceIndex(default, set): Int;
	public var faceValue(get, never): Int;

	/**
		store the position of the die, different from the render position
	**/
	/**
		x
	**/
	public var targetX: Float = 0;

	/**
		y
	**/
	public var targetY: Float = 0;

	public var location: DieLocation = Void;

	public var afterimage: h2d.Bitmap = null;

	public var type(default, set): DieType = null;

	var rerolling: Float = 0;
	var rerollDelta: Float = 0;

	public var isReady(get, null): Bool;

	public function new(initialFace: Int = 0, type: DieType = Normal) {
		super();
		this.type = type;
		this.faceIndex = initialFace;

		final interactive = new h2d.Interactive(30, 30, this);

		interactive.onOver = function(e: hxd.Event) {
			onOver(this, e);
		}
		interactive.onOut = function(e: hxd.Event) {
			onOut(this, e);
		}
		interactive.onClick = function(e: hxd.Event) {
			if (e.button == 1) {
				onRightClick(this, e);
			} else {
				onLeftClick(this, e);
			}
		}
		interactive.onKeyDown = function(e: hxd.Event) {
			onKeyDown(this, e);
		}
		interactive.onPush = function(e: hxd.Event) {
			onPush(this, e);
		}
		interactive.onRelease = function(e: hxd.Event) {
			onRelease(this, e);
		}
		interactive.enableRightButton = true;
		interactive.cursor = Default;
		interactive.propagateEvents = false;
	}

	public function get_isReady(): Bool {
		return this.location == InVillage && this.rerolling == 0;
	}

	public function set_faceIndex(i: Int): Int {
		this.faceIndex = Math.clampI(i, 0, this.faces.length - 1);
		this.face.currentFrame = this.faces[this.faceIndex];
		return this.faceIndex;
	}

	public function getDragAfterImage(): h2d.Bitmap {
		final bm = new h2d.Bitmap(this.face.getFrame());
		bm.alpha = .5;
		this.afterimage = bm;
		this.afterimage.x = this.x;
		this.afterimage.y = this.y;
		return bm;
	}

	public function removeAfterimage() {
		if (this.afterimage == null) return;
		this.afterimage.remove();
		this.afterimage = null;
	}

	public function get_faceValue(): Int {
		return this.faces[this.faceIndex];
	}

	public function match(criteria: MatchCriteria): Bool {
		final faceV = this.faceValue;
		switch (criteria) {
			case Any:
				return true;
			case FaceValue(i):
				return faceV == i;
			case LessThanEqual(i):
				return faceV <= i;
			case MoreThanEqual(i):
				return faceV >= i;
			case Odd:
				return faceV % 2 == 1;
			case Even:
				return faceV % 2 == 0;
			default:
				return false;
		}
	}

	public function reroll(r: hxd.Rand) {
		this.rerolling = 1.0;
		this.faceIndex = r.randomInt(6);
	}

	public function update(r: hxd.Rand, dt: Float) {
		if (this.rerolling != 0) {
			this.rerolling -= dt;
			this.rerollDelta += dt;
			this.rerolling = Math.clampF(this.rerolling, 0, null);
			if (rerollDelta > Constants.RerollFrameDelay) {
				this.rerollDelta -= Constants.RerollFrameDelay;
				this.face.currentFrame = this.faces[r.randomInt(6)];
			}
			if (this.rerolling == 0) {
				this.faceIndex = this.faceIndex;
			}
		}

		if (this.afterimage == null && this.location == InVillage) {
			if (this.targetX > this.x) {
				this.x += dt * MoveSpeed.x;
				if (this.x > this.targetX) this.x = this.targetX;
			} else if (this.targetX < this.x) {
				this.x -= dt * MoveSpeed.x;
				if (this.x < this.targetX) this.x = this.targetX;
			}

			if (this.targetY > this.y) {
				this.y += dt * MoveSpeed.y;
				if (this.y > this.targetY) this.y = this.targetY;
			} else if (this.targetY < this.y) {
				this.y -= dt * MoveSpeed.y;
				if (this.y < this.targetY) this.y = this.targetY;
			}
		}
	}

	public function set_type(t: DieType): DieType {
		if (this.type == t) return this.type;
		this.type = t;
		if (this.face != null) this.face.remove();
		switch (t) {
			case Normal:
				this.faces = NormalDice;
				this.face = Assets.packed.assets.get('die:default').getAnim();
			case Farmer:
				this.faces = FarmerDice;
				this.face = Assets.packed.assets.get('die:farmer').getAnim();
			case Builder:
				this.faces = BuilderDice;
				this.face = Assets.packed.assets.get('die:builder').getAnim();
			case Explorer:
				this.faces = ExplorerDice;
				this.face = Assets.packed.assets.get('die:explorer').getAnim();
			case Forager:
				this.faces = ForagerDice;
				this.face = Assets.packed.assets.get('die:forager').getAnim();
			case Woodcutter:
				this.faces = WoodcutterDice;
				this.face = Assets.packed.assets.get('die:woodcutter').getAnim();
			case Doctor:
				this.faces = DoctorDice;
				this.face = Assets.packed.assets.get('die:doctor').getAnim();
		}
		this.faceIndex = 1;
		this.face.pause = true;
		this.addChild(face);
		return this.type;
	}

	dynamic public function onOver(die: Die, event: hxd.Event) {}

	dynamic public function onOut(die: Die, event: hxd.Event) {}

	dynamic public function onLeftClick(die: Die, event: hxd.Event) {}

	dynamic public function onRightClick(die: Die, event: hxd.Event) {}

	dynamic public function onKeyDown(die: Die, event: hxd.Event) {}

	dynamic public function onPush(die: Die, event: hxd.Event) {}

	dynamic public function onRelease(die: Die, event: hxd.Event) {}
}
