package world;

enum abstract GameState(String) from String to String {
	/**
		Start of the game.
	**/
	public var SettingUp = "SettingUp";

	/**
		Cleaning up phase
	**/
	public var Upkeep = "Upkeep";

	/**
		Player can place die
	**/
	public var Idle = "Idle";

	/**
		In the middle of placing die
	**/
	public var PlacingDie = "PlacingDie";

	/**
		Trigger die effect
	**/
	public var Triggering = "Triggering";

	/**
		Window State
	**/
	public var WindowState = "WindowState";

	/**
		Ending turn
	**/
	public var EndingTurn = "EndingTurn";

	/**
		Gameover
	**/
	public var Gameover = "Gameover";
}
