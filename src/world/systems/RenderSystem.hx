package world.systems;

import zf.ui.Button;
import zf.ui.GenericButton;
import zf.animations.*;

class RenderSystem extends System {
	public var dragLayers: h2d.Layers;
	public var regionLayers: h2d.Layers;
	public var hudLayers: h2d.Layers;
	public var tutorialLayers: h2d.Layers;
	public var villageLayers: h2d.Layers;
	public var animationsLayer: h2d.Layers;
	public var windowLayers: h2d.Layers;

	public var drawLayers: h2d.Layers;

	public var restartGameButton: Button;
	public var endTurnButton: Button;

	public var yearBoard: HudBoard;
	public var foodBoard: HudBoard;
	public var woodBoard: HudBoard;
	public var stoneBoard: HudBoard;

	public function new() {
		super();
		this.drawLayers = new h2d.Layers();

		this.drawLayers.add(this.tutorialLayers = new h2d.Layers(), 40);
		this.drawLayers.add(this.villageLayers = new h2d.Layers(), 50);
		this.drawLayers.add(this.regionLayers = new h2d.Layers(), 75);
		this.drawLayers.add(this.hudLayers = new h2d.Layers(), 150);
		this.drawLayers.add(this.animationsLayer = new h2d.Layers(), 175);
		this.drawLayers.add(this.dragLayers = new h2d.Layers(), 200);
		this.drawLayers.add(this.windowLayers = new h2d.Layers(), 250);
	}

	public function onGameRestart() {
		this.tutorialLayers.removeChildren();
		this.regionLayers.removeChildren();
		this.villageLayers.removeChildren();
		this.animationsLayer.removeChildren();
		this.windowLayers.removeChildren();
	}

	override public function init(world: World) {
		super.init(world);
		setupHud();
	}

	override public function update(dt: Float) {
		if (this.world.state == null) return;
		updateDragDie(dt);
		this.yearBoard.value = '${this.world.state.turn}';
		this.foodBoard.value = '${this.world.state.food}';
		this.woodBoard.value = '${this.world.state.wood}';
		this.stoneBoard.value = '${this.world.state.stone}';
		for (region in this.world.state.actions) {
			region.update(dt);
		}
		for (region in this.world.state.locations) {
			region.update(dt);
		}
		for (region in this.world.state.buildings) {
			region.update(dt);
		}
		for (region in this.world.state.events) {
			region.update(dt);
		}
	}

	function setupHud() {
		{
			this.hudLayers.add(this.yearBoard = new HudBoard("yearIcon"), 10);
			this.yearBoard.x = 15;
			this.yearBoard.y = 10;
			this.hudLayers.add(this.foodBoard = new HudBoard("foodIcon"), 10);
			this.foodBoard.putOnRight(this.yearBoard, [120, 0]);
			this.hudLayers.add(this.woodBoard = new HudBoard("woodIcon"), 10);
			this.woodBoard.putOnRight(this.foodBoard, [10, 0]);
			this.hudLayers.add(this.stoneBoard = new HudBoard("stoneIcon"), 10);
			this.stoneBoard.putOnRight(this.woodBoard, [10, 0]);
		}

		{
			this.endTurnButton = new EndTurnButton();
			this.hudLayers.addChild(this.endTurnButton);
			this.endTurnButton.setX(Globals.game.gameWidth, AlignCenter);
			this.endTurnButton.setY(Globals.game.gameHeight, AnchorBottom, 20);
			this.endTurnButton.onClick = endTurnClick;
		}
		{
			this.restartGameButton = new SmallButton('restart');
			this.hudLayers.addChild(this.restartGameButton);
			this.restartGameButton.setX(Globals.game.gameWidth, AnchorRight, 15).setY(15);
			this.restartGameButton.onClick = restartGameClick;
		}
	}

	function endTurnClick(button: Int) {
		this.world.endTurn();
	}

	function restartGameClick(button: Int) {
		this.world.restartGame();
	}

	function getNumberOfMovingRegion(regions: Array<Region>): Int {
		var count = 0;
		for (r in regions) {
			if (r.x != r.targetX) count += 1;
		}
		return count;
	}

	public function addActionRegion(region: Region, animate: Bool = true) {
		// TODO, fix this to listen to events instead.
		final pt = getActionPosition(this.world.state.actions.length);
		region.targetX = pt.x;
		region.y = region.targetY = pt.y;
		if (animate) {
			region.x = Globals.game.gameWidth;
			region.x += getNumberOfMovingRegion(this.world.state.actions) * (Constants.RegionSize.x + 10);
		} else {
			region.x = region.targetX;
		}
		this.regionLayers.addChild(region);
	}

	public function onLocationAdded(region: Region, animate: Bool = true) {
		final pt = getLocationPosition(this.world.state.locations.length);
		region.targetX = pt.x;
		region.y = region.targetY = pt.y;
		if (animate) {
			region.x = Globals.game.gameWidth;
			region.x += getNumberOfMovingRegion(this.world.state.locations) * (Constants.RegionSize.x + 10);
		} else {
			region.x = region.targetX;
		}
		this.regionLayers.addChild(region);
	}

	public function onBuildingAdded(region: Region, animate: Bool = true) {
		final pt = getBuildingPosition(this.world.state.buildings.length);
		region.targetX = pt.x;
		region.y = region.targetY = pt.y;
		if (animate) {
			region.x = Globals.game.gameWidth;
			region.x += getNumberOfMovingRegion(this.world.state.buildings) * (Constants.RegionSize.x + 10);
		} else {
			region.x = region.targetX;
		}
		this.regionLayers.addChild(region);
	}

	public function onEventAdded(region: Region, animate: Bool = true) {
		final pt = getEventPosition(this.world.state.events.length);
		region.targetX = pt.x;
		region.y = region.targetY = pt.y;
		if (animate) {
			region.x = Globals.game.gameWidth;
			region.x += getNumberOfMovingRegion(this.world.state.events) * (Constants.RegionSize.x + 10);
		} else {
			region.x = region.targetX;
		}
		this.regionLayers.addChild(region);
	}

	public function onLocationRemoved(region: Region) {
		syncLocations();
		this.world.animator.runAnim(new AlphaTo(new WrappedObject(region), 0, 1 / .2), function() {
			region.remove();
		});
	}

	public function onEventRemoved(region: Region) {
		syncEvents();
		this.world.animator.runAnim(new AlphaTo(new WrappedObject(region), 0, 1 / .2), function() {
			region.remove();
		});
	}

	public function onBuildingRemoved(region: Region) {
		syncBuildings();
		this.world.animator.runAnim(new AlphaTo(new WrappedObject(region), 0, 1 / .2), function() {
			region.remove();
		});
	}

	function syncActions() {
		var pt = new Point2f();
		for (ind => region in this.world.state.actions) {
			getActionPosition(ind, pt);
			region.targetX = pt.x;
			region.targetY = pt.y;
		}
	}

	function syncLocations() {
		var pt = new Point2f();
		for (ind => region in this.world.state.locations) {
			getLocationPosition(ind, pt);
			region.targetX = pt.x;
			region.targetY = pt.y;
		}
	}

	function syncBuildings() {
		final pt = new Point2f();
		for (ind => region in this.world.state.buildings) {
			getBuildingPosition(ind, pt);
			region.targetX = pt.x;
			region.targetY = pt.y;
		}
	}

	function syncEvents() {
		var pt = new Point2f();
		for (ind => region in this.world.state.events) {
			getEventPosition(ind, pt);
			region.targetX = pt.x;
			region.targetY = pt.y;
		}
	}

	function getActionPosition(ind: Int, pt: Point2f = null): Point2f {
		final StartX = 15;
		final StartY = 50;
		final x = StartX + (ind * (Constants.RegionSize.x + 10));
		final y = StartY;
		if (pt == null) return [x, y];
		pt.x = x;
		pt.y = y;
		return pt;
	}

	function getLocationPosition(ind: Int, pt: Point2f = null): Point2f {
		final StartX = 15;
		final StartY = 50 + (2 * (Constants.RegionSize.y + 10));
		final x = StartX + (ind * (Constants.RegionSize.x + 10));
		final y = StartY;
		if (pt == null) return [x, y];
		pt.x = x;
		pt.y = y;
		return pt;
	}

	function getBuildingPosition(ind: Int, pt: Point2f = null): Point2f {
		final StartX = 15;
		final StartY = 50 + Constants.RegionSize.y + 10;
		final x = StartX + (ind * (Constants.RegionSize.x + 10));
		final y = StartY;
		if (pt == null) return [x, y];
		pt.x = x;
		pt.y = y;
		return pt;
	}

	function getEventPosition(ind: Int, pt: Point2f = null): Point2f {
		final StartX = 15;
		final StartY = 50 + (3 * (Constants.RegionSize.y + 10));
		final x = StartX + (ind * (Constants.RegionSize.x + 10));
		final y = StartY;
		if (pt == null) return [x, y];
		pt.x = x;
		pt.y = y;
		return pt;
	}

	public function onFoodConsumed(die: Die) {
		final obj = new h2d.Object();
		obj.addChild(Assets.packed.assets.get("icon:food").getBitmap());

		final text = new h2d.Text(Assets.displayFont3x);
		text.text = "-";
		text.x = -3;
		text.y = 2;
		obj.addChild(text);

		animateFloatAboveDie(die, obj);
	}

	public function animateFoodConsumed(dice: Array<Die>, onFinish: Void->Void) {
		if (dice.length == 0) {
			onFinish();
			return;
		}
		final animations: Array<Animation> = [];
		final source: Point2f = [this.foodBoard.x + 10, this.foodBoard.y];

		final animations: Array<Animation> = [];
		final bms = [];
		for (die in dice) {
			// @formatter:off
			final bm = Assets.packed.assets.get("icon:food").getBitmap().setX(source.x).setY(source.y);
			bms.push(bm);
			this.animationsLayer.addChild(bm);
			animations.push(new MoveToLocationByDuration(new WrappedObject(bm), [die.x, die.y], .3));
		}

		this.world.animator.runAnim(new Batch(animations), function() {
			for (bm in bms) bm.remove();
			onFinish();
		});

	}

	public function animateDeaths(dice: Array<Die>, onFinish: Void->Void = null) {
		if (dice.length == 0) {
			if (onFinish != null) onFinish();
			return;
		}
		final animations: Array<Animation> = [];
		for (die in dice) {
			animations.push(new AlphaTo(new WrappedObject(die), 0, 1 / .3));
			animations.push(new MoveByAmountByDuration(new WrappedObject(die), [0, -20], .3));
		}

		this.world.animator.runAnim(new Batch(animations), function() {
			syncVillagersPosition();
			if (onFinish != null) onFinish();
		});
	}

	public function animateStarve(die: Die, onFinish: Void->Void) {
		final obj = new h2d.Object();
		final text = new h2d.Text(Assets.displayFont2x);
		text.text = "Starve";
		text.textColor = Colors.Reds[0];
		obj.addChild(text);
		animateFloatAboveDie(die, obj, onFinish);
	}

	function animateFloatAboveDie(die: Die, obj: h2d.Object, onFinish: Void->Void = null) {
		obj.x = die.x;
		obj.y = die.y - 20;
		this.animationsLayer.addChild(obj);
		// @formatter:off
		this.world.animator.runAnim(
			new MoveByAmountByDuration(new WrappedObject(obj), [0, -20], .3),
			function() {
				obj.remove();
				if (onFinish != null) onFinish();
			}
		);
	}

	function animateFloatAboveDieRegion(region: DieRegion, obj: h2d.Object, onFinish: Void->Void = null) {
		final bounds = region.getBounds();
		obj.x = bounds.xMin;
		obj.y = bounds.yMin - 20;
		this.animationsLayer.addChild(obj);
		// @formatter:off
		this.world.animator.runAnim(
			new MoveByAmountByDuration(new WrappedObject(obj), [0, -20], .3),
			function() {
				obj.remove();
				if (onFinish != null) onFinish();
			}
		);
	}

	public function onRewardCollected(region: DieRegion, reward: Reward, amt: Int) {
		final rewardString = Utils.getRewardString(reward, amt);

		final obj = new h2d.Object();
		final text = new h2d.Text(Assets.displayFont1x);
		text.text = '+${rewardString}';
		obj.addChild(text);
		animateFloatAboveDieRegion(region, obj);
	}

	/**
		Drag logic
	**/
	var draggedDie: Die = null;

	function updateDragDie(dt: Float) {
		if (draggedDie != null) {
			final scene = this.drawLayers.getScene();
			if (scene == null) return;
			final positionX = scene.mouseX;
			final positionY = scene.mouseY;
			draggedDie.x = positionX - 15;
			draggedDie.y = positionY - 15;
		}
	}

	function attachDie(die: Die) {
		die.onPush = startDrag;
		die.onRelease = stopDrag;
		die.onOver = showDieDetails;
		die.onOut = hideDieDetails;
	}

	function startDrag(die: Die, event: hxd.Event) {
		if (this.world.gameState != Idle || this.draggedDie != null) return;
		if (!die.isReady) return;
		hideDieDetails(die, null);
		this.draggedDie = die;
		final afterimage = die.getDragAfterImage();
		die.parent.addChild(afterimage);
		this.dragLayers.addChild(die);
	}

	function stopDrag(die: Die, event: hxd.Event) {
		if (this.world.gameState != Idle) return;
		if (die != draggedDie) return;
		var collisionDieRegion: DieRegion = null;
		var collisionRegion: Region = null;

		final dieBound = die.getBounds();

		final regions: Array<Region> = [];
		regions.pushArray(this.world.state.actions);
		regions.pushArray(this.world.state.buildings);
		regions.pushArray(this.world.state.locations);
		regions.pushArray(this.world.state.events);

		for (region in regions) {
			if (!dieBound.intersects(region.getBounds())) continue;
			collisionRegion = region;
			collisionDieRegion = region.getDieRegion(dieBound);
			break;
		}

		var returnToVillage: Bool = true;

		if (collisionDieRegion != null && collisionDieRegion.canPlace(die)) {
			die.remove(); // remove the die first
			this.world.gameState = PlacingDie;
			collisionDieRegion.place(die, function() {
				this.world.gameState = Idle;
			});
			die.removeAfterimage();
		} else {
			returnDie(die);
		}
		this.draggedDie = null;
	}

	var dieDetails: { die: Die, window: h2d.Object };
	function showDieDetails(die: Die, event: hxd.Event) {
		if (this.draggedDie != null) return;
		if (this.dieDetails != null) this.hideDieDetails(dieDetails.die, null);
		final bound = die.getBounds();
		this.dieDetails = { die: die, window: new DieDetailsWindow(world, die.type) };
		this.windowLayers.addChild(this.dieDetails.window);
		this.dieDetails.window.putAboveBound(bound, [0, 10]);
	}

	function hideDieDetails(die: Die, event: hxd.Event) {
		if (this.dieDetails == null) return;
		if (die != null && die != this.dieDetails.die) return;
		this.dieDetails.window.remove();
		this.dieDetails = null;
	}

	public function returnDiceToVillage(dice: Array<Die>, onFinish: Void->Void) {
		final animations: Array<zf.animations.Animation> = [];
		final onFinishes: Array<Void->Void> = [];
		for (die in dice) {
			switch (die.location) {
				case Void:
					returnDie(die);
					animations.push(new AlphaTo(new WrappedObject(die), 1.0, 1.0 / .2));
					onFinishes.push(function() {
						die.location = InVillage;
					});
					die.alpha = 0;
				case InVillage: // if in village, do nothing
					returnDie(die);
					onFinish();
				case Region(r):
					final bounds = die.getBounds();
					this.animationsLayer.addChild(die);
					r.removeDie(die);
					die.x = bounds.xMin;
					die.y = bounds.yMin;
					animations.push(new MoveToLocationByDuration(new WrappedObject(die),
						[die.targetX, die.targetY], .2));
					onFinishes.push(function() {
						die.location = InVillage;
						this.villageLayers.addChild(die);
					});
			}
		}
		if (onFinish != null) onFinishes.push(onFinish);

		this.world.animator.runAnim(new Batch(animations), function() {
			for (f in onFinishes) {
				f();
			}
		});
	}

	function returnDie(die: Die) {
		die.x = die.targetX;
		die.y = die.targetY;
		if (die.afterimage != null) {
			die.afterimage.parent.addChild(die);
			die.removeAfterimage();
		} else {
			this.villageLayers.addChild(die);
		}
	}

	public function addVillageDie(value: Int = 1): Die {
		final d = new Die(value);
		attachDie(d);
		this.villageLayers.addChild(d);
		d.location = InVillage;
		final pos = getDieLocation(this.world.state.villagers.length);
		d.x = pos.x;
		d.y = pos.y;
		d.targetX = pos.x;
		d.targetY = pos.y;
		// TODO move to world
		this.world.state.villagers.push(d);
		return d;
	}

	public function syncVillagersPosition() {
		var pt = new Point2f();
		for (ind => d in this.world.state.villagers) {
			getDieLocation(ind, pt);
			d.targetX = pt.x;
			d.targetY = pt.y;
		}
	}

	function getDieLocation(index: Int, pt: Point2f = null): Point2f {
		final StartX = 15;
		final StartY = 600;
		if (pt == null) {
			return [StartX + (40 * index), StartY];
		}
		pt.x = StartX + (40 * index);
		pt.y = StartY;
		return pt;
	}

	public function showGameover() {
		final window = new GameoverWindow(this.world);
		this.windowLayers.addChild(window);
		window.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);
	}

	public function showWindow(w: h2d.Object) {
		this.windowLayers.addChild(w);
		final bounds = w.getBounds();
		if (bounds.xMax >= Globals.game.gameWidth) {
			w.x = Globals.game.gameWidth - bounds.width - 15;
		}
	}
}
