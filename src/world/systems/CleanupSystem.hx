package world.systems;

class CleanupSystem extends System {
	var toProcess: Array<Region>;

	var onFinish: Void->Void = null;

	var currentProcess: Region;

	override public function init(world: World) {
		super.init(world);
	}

	override public function update(dt: Float) {
		if (this.world.gameState != EndingTurn) return;
		if (this.currentProcess != null || this.toProcess == null) return;
		if (this.toProcess.length == 0) {
			doneCleanup();
			return;
		}
		this.currentProcess = this.toProcess.shift();

		final dice = this.currentProcess.trigger();
		this.world.returnDiceToVillage(dice, function() {
			this.currentProcess = null;
		});
	}

	public function startCleanup(onFinish: Void->Void) {
		// add all the dice to the process list
		this.toProcess = [];
		this.toProcess.pushArray(this.world.state.actions);
		this.toProcess.pushArray(this.world.state.buildings);
		this.toProcess.pushArray(this.world.state.locations);
		this.toProcess.pushArray(this.world.state.events);
		this.onFinish = onFinish;
	}

	function doneCleanup() {
		if (this.onFinish != null) {
			this.onFinish();
			this.onFinish = null;
			this.toProcess = null;
		}
	}
}
