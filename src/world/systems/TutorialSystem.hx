package world.systems;

import zf.animations.WrappedObject;
import zf.animations.AlphaTo;
import zf.animations.Wait;

class TutorialSystem extends System {
	var drawLayers: h2d.Layers;

	public function new(drawLayers: h2d.Layers) {
		super();
		this.drawLayers = drawLayers;
	}

	override public function init(world: World) {
		super.init(world);
	}

	public function onTurnStart() {
		final turn = this.world.state.turn;
		this.drawLayers.removeChildren();

		var newTutorial = new h2d.Layers();

		if (turn == 1) {
			{
				var hline = drawHorizontalLine(newTutorial, 15, 590, 108);
				var line = drawVerticalLine(newTutorial, 69, 530, 60);

				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = Colors.Grey[1];
				text.text = "Drag the villagers dice to the actions above";
				text.putAbove(line, [0, 6]);
				newTutorial.addChild(text);
			}
			{
				var vline = drawVerticalLine(newTutorial, 117, 10, 30);
				var hline = drawHorizontalLine(newTutorial, vline.x, vline.y + 15, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = Colors.Grey[1];
				text.text = "Year";
				text.putOnRight(hline, [6, -3]);
				newTutorial.addChild(text);
			}
			{
				var vline = drawVerticalLine(newTutorial, 545, 10, 30);
				var hline = drawHorizontalLine(newTutorial, vline.x, vline.y + 15, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = Colors.Grey[1];
				text.text = "Resources: Food, Wood, Stone";
				text.putOnRight(hline, [6, -3]);
				newTutorial.addChild(text);
			}

			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = [
				"Welcome to Dice Kingdom.",
				"Build your kingdom by managing your villagers.",
				"",
				"Basic: ",
				"- 1 Food is consumed per villager per year.",
				"- Dice are returned after every year, except in some cases.",
				"- When there are not enough food, villagers will die of starvation.",
				"- When you have no villagers, your kingdom is over.",
				"- Don't train all your villagers, or you may not be able to get new villagers.",
			].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.y = 50;
			fullTutorialText.x = 700;
			fullTutorialText.maxWidth = 550;
			newTutorial.addChild(fullTutorialText);

			{
				final bound = this.world.renderSystem.endTurnButton.getBounds();
				var vline = drawVerticalLine(newTutorial, bound.xMin - 6, bound.yMin, Std.int(bound.height));
				var hline = drawHorizontalLine(newTutorial, vline.x - 20, vline.y + bound.height / 2, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = Colors.Grey[1];
				text.text = "Click this to end the year.";
				text.putOnLeft(hline, [6, -3]);
				newTutorial.addChild(text);
			}

			var prev: h2d.Object = null;
			{
				final obj = new h2d.Object();
				var ind = 0;
				inline function addCriteria(criteria: MatchCriteria) {
					final r = new MatchDieRegion(null, criteria);
					r.x = ind * 40;
					obj.addChild(r);
					ind += 1;
				}
				addCriteria(FaceValue(1));
				addCriteria(FaceValue(2));
				addCriteria(FaceValue(3));
				addCriteria(FaceValue(4));
				addCriteria(FaceValue(5));
				addCriteria(FaceValue(6));
				obj.putBelow(fullTutorialText, [0, 30]);
				newTutorial.addChild(obj);

				final bound = obj.getBounds();
				drawHorizontalLine(newTutorial, obj.x, obj.y + 40, Std.int(bound.width));
				final line = drawVerticalLine(newTutorial, obj.x + bound.width / 2, obj.y + 40, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.text = ["These require exact match."].join("\n");
				text.textColor = Colors.Grey[1];
				text.putBelow(line, [0, 6]);
				newTutorial.addChild(text);
				prev = obj;
			}

			{
				final obj = new h2d.Object();
				var ind = 0;
				inline function addCriteria(criteria: MatchCriteria) {
					final r = new MatchDieRegion(null, criteria);
					r.x = ind * 40;
					obj.addChild(r);
					ind += 1;
				}

				addCriteria(LessThanEqual(3));
				addCriteria(Odd);

				newTutorial.addChild(obj);
				obj.putBelow(prev, [0, 40]);
				prev = obj;

				final bound = obj.getBounds();
				drawHorizontalLine(newTutorial, obj.x, obj.y + 40, Std.int(bound.width));
				final line = drawVerticalLine(newTutorial, obj.x + bound.width / 2, obj.y + 40, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.text = ["These require dice to match the critera."].join("\n");
				text.textColor = Colors.Grey[1];
				text.putBelow(line, [0, 6]);
				newTutorial.addChild(text);
				prev = obj;
			}

			{
				final obj = new h2d.Object();
				newTutorial.addChild(obj);

				obj.addChild(new MatchDieRegion(null, Any));
				obj.addChild(new MatchDieRegion(null, Any, "+1").setX(40));
				obj.addChild(new SumDieRegion(null, 15).setX(80));

				newTutorial.addChild(obj);
				obj.putBelow(prev, [0, 120]);

				var hline = drawHorizontalLine(newTutorial, obj.x + 80, obj.y - 6, Std.int(34));
				final line = drawVerticalLine(newTutorial, hline.x + 17, hline.y - 20, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.textColor = Colors.Grey[1];
				text.text = ["Dice placed on these will deplete the number."].join("\n");
				text.putAbove(line, [0, 6]);
				newTutorial.addChild(text);

				final bound = obj.getBounds();
				drawHorizontalLine(newTutorial, obj.x, obj.y + 40, Std.int(bound.width));
				final line = drawVerticalLine(newTutorial, obj.x + bound.width / 2, obj.y + 40, 20);

				final text = new h2d.Text(Assets.displayFont1x);
				text.text = ["These allow any dice to be placed."].join("\n");
				text.textColor = Colors.Grey[1];
				text.putBelow(line, [0, 6]);
				newTutorial.addChild(text);
				prev = obj;
			}

			{
				final obj = new h2d.Object();
				newTutorial.addChild(obj);
				prev = obj;
			}
		} else if (turn == 2) {
			var line = drawVerticalLine(newTutorial, 550, 180, 140);
			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = [
				"The farm and toolshed is almost broken.",
				"It will be a good idea to start building a new one.",
			].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.putBelow(line, [0, 6]);
			fullTutorialText.maxWidth = 500;
			newTutorial.addChild(fullTutorialText);
		} else if (turn == 3) {
			var line = drawVerticalLine(newTutorial, 750, 180, 140);
			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = [
				"Specialise your villagers by training them.",
				"",
				"Each specialisation will have different face values.",
				"",
				"Hover over them to see their effects.",
			].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.putBelow(line, [0, 6]);
			fullTutorialText.maxWidth = 500;
			newTutorial.addChild(fullTutorialText);
		} else if (turn == 4) {
			var line = drawVerticalLine(newTutorial, 950, 180, 140);
			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = ["You can reroll the die by eating.", "This will cost you one food.",].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.putBelow(line, [0, 6]);
			fullTutorialText.maxWidth = 500;
			newTutorial.addChild(fullTutorialText);
		} else if (turn == 5) {
			final bounds = this.world.renderSystem.restartGameButton.getBounds();
			var line = drawVerticalLine(newTutorial, bounds.xMin + bounds.width / 2, bounds.yMax + 5, 180);
			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = ["Restart game if you want"].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.putBelow(line, [0, 6]);
			fullTutorialText.maxWidth = 500;
			fullTutorialText.setX(Globals.game.gameWidth, AnchorRight, 15);
			newTutorial.addChild(fullTutorialText);
		} else if (turn == 8) {
			var vline = drawVerticalLine(newTutorial, 221, 455, 125);
			var hline = drawHorizontalLine(newTutorial, vline.x, vline.y + 60, 20);
			final fullTutorialText = new h2d.Text(Assets.displayFont2x);
			fullTutorialText.text = [
				"Events will happen from time to time.",
				"There are good events and bad events.",
			].join("\n");
			fullTutorialText.textColor = Colors.Grey[1];
			fullTutorialText.putOnRight(hline, [0, 6]);
			fullTutorialText.maxWidth = 500;
			newTutorial.addChild(fullTutorialText);
		}

		newTutorial.alpha = 0;
		if (turn == 1) {
			this.world.animator.runAnim(new Wait(1).then(new AlphaTo(new WrappedObject(newTutorial), 1,
				1 / .4)));
		} else {
			this.world.animator.runAnim(new AlphaTo(new WrappedObject(newTutorial), 1, 1 / .4));
		}
		this.drawLayers.addChild(newTutorial);
	}

	function drawHorizontalLine(layer: h2d.Layers, x: Float, y: Float, width: Int) {
		final bm = Assets.fromColor(Colors.Grey[1], width, 2);
		layer.addChild(bm);
		bm.x = x;
		bm.y = y;
		return bm;
	}

	function drawVerticalLine(layer: h2d.Layers, x: Float, y: Float, height: Int) {
		final bm = Assets.fromColor(Colors.Grey[1], 2, height);
		layer.addChild(bm);
		bm.x = x;
		bm.y = y;
		return bm;
	}
}
