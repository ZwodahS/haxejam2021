package world.systems.ui;

class HudBoard extends h2d.Layers {
	public var label: h2d.Object;
	public var valueText: h2d.Text;
	public var value(get, set): String;

	public function new(labelString: String) {
		super();
		this.addChild(Assets.packed.assets.get("hud:bg").getBitmap());

		this.addChild(this.label = Assets.packed.assets.get('hud:${labelString}').getBitmap());
		this.label.setX(10);

		this.addChild(this.valueText = new h2d.Text(Assets.displayFont3x));
		this.valueText.textColor = Colors.Browns[1];
		this.valueText.maxWidth = 64;
		this.valueText.textAlign = Center;
		this.valueText.setX(32).setY(7);
	}

	public function get_value(): String {
		return this.valueText.text;
	}

	public function set_value(v: String): String {
		return this.valueText.text = v;
	}
}
