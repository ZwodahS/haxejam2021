package world.systems.ui;

import world.Die;

class DieDetailsWindow extends h2d.Layers {
	public static final WindowSize: Point2i = [240, 80];

	public function new(world: World, dieType: DieType) {
		super();
		this.addChild(Assets.whiteBoxFactory.make(WindowSize));

		final typeTitle = new h2d.Text(Assets.displayFont1x);
		typeTitle.text = '${dieType} Villager';
		typeTitle.textColor = Colors.Black[0];
		typeTitle.y = 5;
		typeTitle.x = 5;
		addChild(typeTitle);

		// @formatter:off
		final frames = switch (dieType) {
			case Normal: Assets.packed.assets.get('die:default').getTiles();
			case Farmer: Assets.packed.assets.get('die:farmer').getTiles();
			case Builder: Assets.packed.assets.get('die:builder').getTiles();
			case Explorer: Assets.packed.assets.get('die:explorer').getTiles();
			case Forager: Assets.packed.assets.get('die:forager').getTiles();
			case Woodcutter: Assets.packed.assets.get('die:woodcutter').getTiles();
			case Doctor: Assets.packed.assets.get('die:doctor').getTiles();
			default: Assets.packed.assets.get('die:default').getTiles();
		}
		// @formatter:off
		final faces = switch (dieType) {
			case Normal: Die.NormalDice;
			case Farmer: Die.FarmerDice;
			case Builder: Die.BuilderDice;
			case Explorer: Die.ExplorerDice;
			case Forager: Die.ForagerDice;
			case Woodcutter: Die.WoodcutterDice;
			case Doctor: Die.DoctorDice;
			default: Die.NormalDice;
		}

		for (ind => faceValue in faces) {
			final bm = new h2d.Bitmap(frames[faceValue]);
			bm.x = 5 + (40 * ind);
			bm.y = 25;
			addChild(bm);
		}

		final additionalText = new h2d.Text(Assets.bodyRegularFont1x);
		additionalText.text = getAdditionalText(dieType);
		additionalText.textColor = Colors.Black[0];
		additionalText.y = 60;
		additionalText.x = 5;
		addChild(additionalText);
	}

	function getAdditionalText(dieType: DieType): String {
		switch(dieType) {
			case Farmer:
				return "Double points when farming";
			case Explorer:
				return "Does not use building";
			default:
				return "";
		}
		return "";
	}
}
