package world.systems.ui;

class SmallButton extends zf.ui.Button {
	var bitmap: h2d.Object;

	public function new(id: String) {
		super(30, 30);
		this.addChild(this.bitmap = Assets.packed.assets.get('button:${id}').getBitmap());
	}

	override function updateButton() {
		if (this.isOver) {
			this.bitmap.y = -4;
		} else {
			this.bitmap.y = 0;
		}
	}
}
