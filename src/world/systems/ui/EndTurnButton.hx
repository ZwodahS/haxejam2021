package world.systems.ui;

class EndTurnButton extends zf.ui.Button {
	var obj: h2d.Object;

	public function new() {
		final bitmap = Assets.packed.assets.get('button:endturn').getBitmap();
		final bound = bitmap.getBounds();
		super(Std.int(bound.width), Std.int(bound.height));
		this.obj = new h2d.Object();
		final text = new h2d.Text(Assets.displayFont3x);
		text.text = "End Turn";
		text.setX(bound.width, AlignCenter).setY(bound.height, AlignCenter);
		this.obj.addChild(bitmap);
		this.obj.addChild(text);

		this.addChild(this.obj);
	}

	override function updateButton() {
		if (this.isOver) {
			this.obj.y = -4;
		} else {
			this.obj.y = 0;
		}
	}
}
