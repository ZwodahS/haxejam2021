package world.systems.ui;

import zf.ui.GenericButton;

class GameoverWindow extends h2d.Layers {
	public static final WindowSize: Point2i = [400, 400];

	public var world: World;

	public var restartButton: GenericButton;

	public function new(world: World) {
		super();
		this.world = world;
		this.addChild(Assets.brownBoxFactory.make(GameoverWindow.WindowSize));

		final title = new h2d.Text(Assets.displayFont4x);
		title.text = 'Gameover';
		title.setX(WindowSize.x, AlignCenter).setY(10);
		this.addChild(title);

		final description = new h2d.HtmlText(Assets.bodyBoldFont2x);
		final strings: Array<String> = [];
		strings.push('Year Survived: ${this.world.state.turn}');
		strings.push('Peak Population: ${this.world.state.peakPopulation}');
		strings.push('Villagers Death: ${this.world.state.death}');
		description.text = strings.join("<br />");
		description.setX(20).setY(50);
		this.addChild(description);

		final buttonSize: Point2i = [150, 45];
		final objs = [
			Assets.brownBoxFactory.make(buttonSize),
			Assets.brownBoxFactory.make(buttonSize),
			Assets.brownBoxFactory.make(buttonSize),
			Assets.brownBoxFactory.make(buttonSize)
		];
		this.restartButton = new GenericButton(objs[0], objs[1], objs[2], objs[3]);
		this.addChild(this.restartButton);
		this.restartButton.setX(WindowSize.x, AlignCenter);
		this.restartButton.setY(WindowSize.y, AnchorBottom, 10);
		this.restartButton.font = Assets.displayFont3x;
		this.restartButton.text = "Restart";
		this.restartButton.onClick = function(e) {
			this.world.restartGame();
			this.remove();
		}
	}
}
