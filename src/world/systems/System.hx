package world.systems;

import zf.MessageDispatcher;

/**
	Define a generic system object.

	Mon 15:59:37 18 Oct 2021
	Unlike ECS, there is no Entity. We are better off creating a System that doesn't deal with Entity.
	The idea is to use System to group functionality together.
**/
class System {
	public var world(default, null): World;
	public var dispatcher(default, null): MessageDispatcher;

	public function new() {}

	public function init(world: World) {
		this.world = world;
		this.dispatcher = world.dispatcher;
	}

	public function update(dt: Float) {}
}
