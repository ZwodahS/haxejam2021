package world.systems;

class EventSystem extends System {
	public static final IllnessStart = 30;
	public static final EventTypes = ["fire", "rat", "rot"];

	override public function init(world: World) {
		super.init(world);
	}

	public function onTurnStart() {
		// @formatter:off
		final events = getRandomEvents(this.world.state.turn);
		for (event in events) {
			this.world.addEventRegion(event);
		}

		if (this.world.state.turn == 2) this.world.addActionRegion(new BuildAction(this.world));
		if (this.world.state.turn == 3) this.world.addActionRegion(new TrainAction(this.world));
		if (this.world.state.turn == 4) this.world.addActionRegion(new EatAction(this.world));
	}

	function getRandomEvents(turn: Int): Array<EventRegion> {
		final events: Array<EventRegion> = [];
		if (turn == 8) events.push(new MigrationEvent(this.world));
		if (turn == 30) events.push(new Plague(this.world));
		if (turn == IllnessStart || turn == IllnessStart + 5) events.push(new Illness(this.world));
		if (turn == IllnessStart + 10 || turn == IllnessStart + 15) events.push(new SeriousIllness(this.world));
		if (turn >= IllnessStart + 20) events.push(new DeathBed(this.world));
		if (turn >= IllnessStart + 30) events.push(new DeathBed(this.world));

		if (turn >= 10 && turn % 5 == 0) {
			final type = this.world.r.randomChoice(EventTypes);
			switch(type) {
				case "fire":
					events.push(new Fire(this.world));
				case "rat":
					events.push(new RatInfestation(this.world));
				case "rot":
					events.push(new RottingWood(this.world));
			}
		}
		return events;
	}
}
